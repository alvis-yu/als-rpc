/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.springbean
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-05 2:49 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-05 2:49 PM
 */


public class RpcConfig {

    public static Integer getPort() {
        return 8787;
    }

    public static String getZookeeper() {
        return "192.168.1.61:2181,192.168.1.62:2181,192.168.1.63:2181";
    }

    public static String getBasePackage() {
        return "com.alvis.demo";
    }
}