/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.RpcMethod
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-03 10:04 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.demo;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 10:04 AM
 */
public interface Test2 {

    /**
     * gogo
     * @return Integer
     */
    Integer gogo();
}
