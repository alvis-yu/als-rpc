/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.RpcMethod.impl
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-03 10:05 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.demo.impl;

import com.alvis.demo.Test2;
import org.springframework.stereotype.Service;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 10:05 AM
 */

@Service
public class Test2Impl implements Test2 {
    @Override
    public Integer gogo() {
        return 555;
    }
}
