/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.RpcMethod.impl
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-03 10:05 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.demo.impl;


import com.alvis.config.RpcConfig;
import com.alvis.demo.Test;
import com.alvis.container.RpcService;
import com.alvis.demo.TestDto;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 10:05 AM
 */


@RpcService("com.alvis.RpcMethod.RpcTest")
public class TestImpl implements Test {

    @Autowired
    private com.alvis.demo.Test2 test2;

    @Override
    public String test(String parameter1, Integer parameter2) {
        return parameter1 + " " + parameter2 + " " + gogo();
    }

    @Override
    public TestDto test2() {
        TestDto testDto = new TestDto();
        testDto.setPara1(RpcConfig.getPort().toString());
        testDto.setPara2(2);
        return testDto;
    }

    @Override
    public Integer gogo() {
        return test2.gogo();
    }
}
