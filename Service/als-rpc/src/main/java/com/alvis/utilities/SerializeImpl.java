/**
 * @Project Name :  aframework
 * @Package Name :  com.core.serialization
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-03-12 9:41 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-03-12 9:41 AM
 */

public class SerializeImpl implements Serialize {

    @Override
    public <T> T convertToObject(String json, Class<T> classOfT) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(json,classOfT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> String convertToString(T object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
