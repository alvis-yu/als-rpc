/**
 * @Project Name :  aframework
 * @Package Name :  com.core.serialization
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-03-12 9:41 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.utilities;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-03-12 9:41 AM
 */
public interface Serialize {

    /**
     * convertToObject
     * @param json
     * @param classOfT
     * @param <T>
     * @return T
     */
    <T> T convertToObject(String json, Class<T> classOfT);

    /**
     * convertToString
     * @param object
     * @param <T>
     * @return T
     */
    <T> String convertToString(T object);
}
