/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.RpcMethod
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-03 10:26 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.container.spring;

import com.alvis.config.RpcConfig;
import com.alvis.utilities.Component;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 10:26 AM
 */
public class SpringApplication implements Component {


    private static AnnotationConfigApplicationContext annotationApplicationContext;

    @Override
    public void start() {
        annotationApplicationContext = new AnnotationConfigApplicationContext();
        annotationApplicationContext.getBeanFactory().addBeanPostProcessor(new RpcServiceAnnotationProcessor());
        annotationApplicationContext.scan(RpcConfig.getBasePackage());
        annotationApplicationContext.refresh();
        annotationApplicationContext.start();
    }


    @Override
    public void stop() {
        annotationApplicationContext.stop();
        annotationApplicationContext.close();
    }


    public static AnnotationConfigApplicationContext getAnnotationApplicationContext() {
        return annotationApplicationContext;
    }


}
