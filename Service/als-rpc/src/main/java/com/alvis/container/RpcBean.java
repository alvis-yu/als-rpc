/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.container
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-06 10:01 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.container;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-06 10:01 AM
 */
public interface RpcBean {

    /**
     * getBeanByType
     * @param requiredType
     * @param <T>
     * @return T
     */
    <T> T getBeanByType(Class<T> requiredType);

    /**
     * getBeanByName
     * @param name
     * @return Object
     */
    Object getBeanByName(String name);
}
