/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.container.spring
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-06 10:01 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.container.spring;

import com.alvis.container.RpcBean;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-06 10:01 AM
 */
public class SpringBeanImpl implements RpcBean {

    public SpringBeanImpl(AnnotationConfigApplicationContext annotationApplicationContext) {
        this.annotationApplicationContext = annotationApplicationContext;
    }

    private AnnotationConfigApplicationContext annotationApplicationContext;

    @Override
    public <T> T getBeanByType(Class<T> requiredType) {
        return annotationApplicationContext.getBean(requiredType);
    }

    @Override
    public Object getBeanByName(String name) {
        return annotationApplicationContext.getBean(name);
    }
}
