/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.springbean
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-03 2:18 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.container.spring;

import com.alvis.container.RpcService;
import com.alvis.registry.zookeeper.RegisterManager;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;


import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 2:18 PM
 */

@Component
public class RpcServiceAnnotationProcessor implements BeanPostProcessor {


    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        Class clazz = bean.getClass();
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof RpcService) {
                RpcService rpcService = (RpcService) annotation;
                RegisterManager.getZkServiceNode().addNode(rpcService.value());
            }
        }
        return bean;
    }

}