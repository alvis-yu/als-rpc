package com.alvis;

import com.alvis.container.spring.SpringApplication;
import com.alvis.registry.zookeeper.RegisterManager;
import com.alvis.transfer.RpcServer;
import com.alvis.utilities.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 * @author Alvis
 */

public class Main {
    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        logger.info("rpc server is starting ... ");

        Component registerManager = new RegisterManager();
        registerManager.start();

        Component springApplication = new SpringApplication();
        springApplication.start();

        Component rpcServer = new RpcServer();
        rpcServer.start();

    }
}
