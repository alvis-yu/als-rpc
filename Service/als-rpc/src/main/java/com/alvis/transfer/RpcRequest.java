/**
 * @Project Name :  als-rpc-client
 * @Package Name :  com.alvis.transfer
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-04 1:44 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.transfer;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-04 1:44 PM
 */
public class RpcRequest implements Serializable {

    private static final long serialVersionUID = 3228645871631140531L;

    String serviceName;
    String methodName;
    Object[] args;
    Class<?>[] parameterTypes;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }


}

