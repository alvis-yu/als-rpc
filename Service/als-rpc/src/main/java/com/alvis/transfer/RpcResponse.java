/**
 * @Project Name :  als-rpc-client
 * @Package Name :  com.alvis.transfer
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-04 1:44 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.transfer;

import java.io.Serializable;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-04 1:44 PM
 */
public class RpcResponse implements Serializable {

    private static final long serialVersionUID = 5592944630790583330L;
    private String serviceName;
    private Object response;

    public RpcResponse(String serviceName, Object response) {
        this.serviceName = serviceName;
        this.response = response;
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

}

