package com.alvis;

import static org.junit.Assert.assertTrue;

import com.alvis.utilities.Serialize;
import com.alvis.utilities.SerializeImpl;
import com.alvis.transfer.RpcRequest;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {

        Serialize serialization = new SerializeImpl();
        RpcRequest rpcRequest = serialization.convertToObject("{\"serviceName\":\"com.alvis.RpcMethod.RpcTest\",\"methodName\":\"test\",\"parameterTypes\":[\"java.lang.String\",\"java.lang.Integer\"],\"args\":[\"hello\",123]}", RpcRequest.class);

        assertTrue( true );
    }
}
