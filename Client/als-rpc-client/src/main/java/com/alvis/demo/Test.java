/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.RpcMethod
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-03 10:04 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.demo;

import com.alvis.container.RpcService;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 10:04 AM
 */

@RpcService("com.alvis.RpcMethod.RpcTest")
public interface Test {

    /**
     * test
     *
     * @param parameter1
     * @param parameter2
     * @return String
     */
    String test(String parameter1, Integer parameter2);

    /**
     * test2
     *
     * @return TestDto
     */
    TestDto test2();

    /**
     * gogo
     *
     * @return Integer
     */
    Integer gogo();

    /**
     * gogo2
     *
     * @param parameter
     * @return Integer
     */
    Integer gogo2(Integer parameter);
}
