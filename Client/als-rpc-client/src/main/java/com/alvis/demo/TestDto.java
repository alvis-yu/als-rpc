/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.demo
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-07 6:07 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.demo;

import java.io.Serializable;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-07 6:07 PM
 */
public class TestDto implements Serializable {

    private static final long serialVersionUID = -3101338943155067648L;
    private String para1;

    public String getPara1() {
        return para1;
    }

    public void setPara1(String para1) {
        this.para1 = para1;
    }

    public Integer getPara2() {
        return para2;
    }

    public void setPara2(Integer para2) {
        this.para2 = para2;
    }

    private Integer para2;
}
