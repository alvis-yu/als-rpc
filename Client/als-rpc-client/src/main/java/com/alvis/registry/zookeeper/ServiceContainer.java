/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.registry.zookeeper
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-06 9:36 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.registry.zookeeper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-06 9:36 AM
 */
public class ServiceContainer {

    private static List<String> serviceNames = new ArrayList<>();

    public static void addService(String serviceName) {
        serviceNames.add(serviceName);
    }

    public static List<String> getServiceNames() {
        return serviceNames;
    }


    public static void setServiceNames(List<String> serviceNames) {
        ServiceContainer.serviceNames = serviceNames;
    }

}
