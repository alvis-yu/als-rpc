package com.alvis;

import com.alvis.container.RpcBean;
import com.alvis.container.spring.SpringApplication;
import com.alvis.container.spring.SpringBeanImpl;
import com.alvis.demo.Test;
import com.alvis.demo.TestDto;
import com.alvis.registry.zookeeper.RegisterManager;
import com.alvis.utilities.Component;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Hello world!
 *
 * @author Alvis
 */
public class Main {
    public static void main(String[] args) {

        Component zkRegisterApplication = new RegisterManager();
        zkRegisterApplication.start();

        Component springApplication = new SpringApplication();
        springApplication.start();

        RpcBean rpcBean = new SpringBeanImpl(SpringApplication.getAnnotationApplicationContext());
        Test test = (Test) rpcBean.getBeanByName("com.alvis.RpcMethod.RpcTest");
        IntStream.range(1, 2).forEach(d -> {
            System.out.println(test.test("12", 2));
            /*TestDto testDto = test.test(1,2);
            System.out.println(testDto.getPara1());*/
        });


        /*RpcBean rpcBean = new SpringBeanImpl(SpringApplication.getAnnotationApplicationContext());
        Test test = (Test) rpcBean.getBeanByName("com.alvis.RpcMethod.RpcTest");
        TestDto testDto = test.test2();

        System.out.println(testDto.getPara1());

        String str = "第一次传输";
        String result = test.test(str, 123);
        System.out.println(result);

        result = test.test(result, 123);
        System.out.println(result);

        result = test.test(result, 123);
        System.out.println(result);

        result = test.test(result, 123);
        System.out.println(result);*/

        try {
            TimeUnit.DAYS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        zkRegisterApplication.stop();
        springApplication.stop();
    }
}
