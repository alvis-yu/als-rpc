/**
 * @Project Name :  als-rpc-client
 * @Package Name :  com.alvis.transfer
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-04 4:19 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.transfer;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-04 4:19 PM
 */
public class RpcTransfer {

    private RpcResponse response;
    private RpcRequest request;

    public RpcRequest getRequest() {
        return request;
    }

    public void setRequest(RpcRequest request) {
        this.request = request;
    }

    public RpcResponse getResponse() {
        return response;
    }

    public void setResponse(RpcResponse response) {
        this.response = response;
    }


}
