/**
 * @Project Name :  als-rpc-client
 * @Package Name :  com.alvis.transfer
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-04 3:42 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.transfer;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-04 3:42 PM
 */
public class ClientHandler extends ChannelInboundHandlerAdapter {

    RpcTransfer nettyTransfer;

    public ClientHandler(RpcTransfer nettyTransfer) {
        this.nettyTransfer = nettyTransfer;
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        RpcResponse response = (RpcResponse) msg;
        nettyTransfer.setResponse(response);
        ctx.close();
    }


    /**
     * @param ctx
     * @throws Exception connected adn then send message
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(nettyTransfer.getRequest());
    }


}
