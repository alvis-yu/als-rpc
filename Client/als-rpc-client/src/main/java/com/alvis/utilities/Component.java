/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.utilities
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-06 9:17 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.utilities;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-06 9:17 AM
 */
public interface Component {

    /**
     * start
     */
    void start();

    /**
     * stop
     */
    void stop();
}
