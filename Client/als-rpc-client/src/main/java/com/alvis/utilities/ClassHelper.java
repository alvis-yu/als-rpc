/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.utilities
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-05 10:19 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.utilities;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-05 10:19 AM
 */
public class ClassHelper {
    public static Class<?>[] fullNameToClass(String[] parameterTypes) {
        Class<?>[] pClass = new Class<?>[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++) {
            try {
                pClass[i] = Class.forName(parameterTypes[i]);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return pClass;
    }

    public static String[] classToFullName(Class<?>[] parameterTypes) {
        String[] pstr = new String[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++) {
            pstr[i] = parameterTypes[i].getName();
        }
        return pstr;
    }
}
