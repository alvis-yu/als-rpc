/**
 * @Project Name :  als-rpc-client
 * @Package Name :  com.alvis.springscan
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-04 9:12 AM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.container.spring;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 10:26 AM
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({RpcServiceBeanRegistrar.class})
public @interface EnableRegistrarConfig {

}
