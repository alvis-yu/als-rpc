/**
 * @Project Name :  als-rpc
 * @Package Name :  com.alvis.springbean
 * @Description :  TODO
 * @author :  Alvis
 * @Creation Date:  2018-07-03 2:17 PM
 * @ModificationHistory Who    When    What
 * --------  ---------  --------------------------
 */
package com.alvis.container;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author :  Alvis
 * @Description :  TODO
 * @Creation Date:  2018-07-03 2:17 PM
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface RpcService {

    /**
     * The value may indicate a suggestion for a logical component name,
     * to be turned into a Spring bean in case of an autodetected component.
     * @return the suggested component name, if any (or empty String otherwise)
     */
    @AliasFor(annotation = Component.class)
    String value() default "";

}